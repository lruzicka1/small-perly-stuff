use strict;
use warnings;
use Data::Dumper qw( Dumper );

#my @alphabet = qw( a b c d e f g h i j k l m n o p q r s t u v w x y z , . : ; ? " - _ X);
my @alphabet = qw( a b c d e f g h i j k l m n o p q r s t u v w x y z X);

sub idx {
    # Returns the index of the very first occurence of $element in the $list.
    my ($element, $list) = @_;
    my $counter = 0;
    foreach (@$list) {
        if ($element eq $_) {
            last;
        }
        $counter++;
    }
    return $counter;
}

sub set_alpha { 
    # This takes a letter to set the alphabet around, so that it
    # starts with that letter continues to the end and then starts
    # from the very beginning.
    my ($letter, $alpha) = @_;
    my @alphabet = @$alpha;
    my $index = idx($letter, \@alphabet);
    my @first = splice(@alphabet, 0, $index);
    my @set = (@alphabet, @first);
    #print(Dumper(@set));
    return \@set;
}

sub set_wheels {
    # Use a password and the alphabet to set the wheel
    # according to the password and return the wheels.
    my ($password, $alpha) = @_;
    my @wheels;
    foreach my $letter (split('', $password)) {
        my $i = idx($letter, $alpha); 
        my $new_alpha = set_alpha($letter, $alpha);
        push(@wheels, $new_alpha);
    }
    #print(Dumper(@wheels));
    return \@wheels;
}

sub get_wheel_conversion {
    # See how much the alphabets are shifted and return
    # the offset numbers from the wheels.
    my $w = shift;
    my @wheels = @$w;
    my @conversion;
    foreach my $wheel (@wheels) {
        my $i = idx('a', $wheel);
        push(@conversion, $i);
    }
    #print(Dumper(@conversion));
    return \@conversion;
}


sub encode {
    # Encode the message according to the shifted alphabets.
    my ($message, $conversion) = @_;
    my $counter = 0;
    my $limit = scalar @$conversion; # The number of converted alphabets is the limit.
    my @encoded;
    # Go over single letter in the encoded message and decode it based on the info.
    foreach my $letter (split('',$message)) {
        if ($letter eq " ") { # We convert spaces into something else.
            $letter = 'X';
        }
        my $alpha = set_alpha($letter, \@alphabet);
        my $step = $conversion->[$counter];
        my $new = $alpha->[$step];
        push(@encoded, $new);
        $counter++;
        if ($counter >= $limit) {
            $counter = 0;
        }
    }
    $counter = 0;
    my $enctext = "";
    foreach my $letter (@encoded) {
        if ($counter >= $limit) {
            $enctext = $enctext ." ";
            $counter = 0;
        }
        $enctext = $enctext . $letter;
        $counter++;
    }
    return $enctext;
}

sub decode {
    my ($message, $conversion) = @_;
    my $limit = (scalar @$conversion - 1);
    my $counter = 0;
    my @reversed = @$conversion;
    my $dectext = "";
    foreach my $letter (split('', $message)) {
        if ($letter eq " ") {
            next;
        }
        my $step = $reversed[$counter];
        my $i = idx($letter, \@alphabet);
        my $position = $i - $step;
        my $decletter = $alphabet[$position];
        if ($decletter eq "X") {
            $decletter = ' ';
        }
        $dectext = $dectext . $decletter;
        $counter++;
        if ($counter > $limit) {
            $counter = 0;
        }
    }
    return($dectext);
}


#------------------------------------------

print("Do you want to encode or decode the message (e/d): ");
my $decision = <STDIN>;
chomp($decision);

print("Type in the password to code/encode the text: ");
my $password = <STDIN>;
chomp($password);

if ($decision eq "e" or $decision eq "encode") {
    $decision = "encode";
    print("Type in the message to be encoded. Basic latin letter only!: ");
}
else {
    $decision = "decode";
    print("Type in the message to be decoded: ");
}

my $message = <STDIN>;
chomp($message);

my $cipher_wheels = set_wheels($password, \@alphabet);
my $conversion_sequence = get_wheel_conversion($cipher_wheels);

my $converted_message;
if ($decision eq "encode") {
    $converted_message = encode($message, $conversion_sequence);
}
else {
    $converted_message = decode($message, $conversion_sequence);
}

print("----- The message after conversion: --------------------\n");
print("$converted_message\n");
